﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SWCLI
{
    public class Starship
    {
        [JsonProperty("name")]
        public string starshipName { get; set; }
    }

    public class StarshipResponse
    {
        [JsonProperty("results")]
        public List<Starship> results  { get; set; }
    }

}
