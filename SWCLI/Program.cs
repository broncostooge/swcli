using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SWCLI
{
    class Program
    {
        //Set a const string to the enpoint we want to use. For now since we ONLY want starships we will use the starships endpoint. In the future, when
        //requesting json data from other endpoints we can make this generic then add the correct endpoint when we make the request
        private const string URLEndpoint = "https://swapi.dev/api/starships";

        static async Task Main(string[] args)
        {
            //try catch for error handling
            try
            {
                using (HttpClient client = new HttpClient())
                using (HttpResponseMessage response = await client.GetAsync(URLEndpoint))
                using (HttpContent content = response.Content)
                {
                    //Get result from request
                    string result = await content.ReadAsStringAsync();

                    //Convert result to a JSON object so we can print at the end
                    dynamic json = JsonConvert.DeserializeObject(result);

                    //Convert JSON response to a class/model of the Starship data. Since we only want the name of the starship, we can use a class
                    //or model to easily parse the info for us.
                    StarshipResponse starshipResponse = JsonConvert.DeserializeObject<StarshipResponse>(result);

                    //Loop through the Starship Response, and output the names
                    for (int i = 0; i < starshipResponse.results.Count; i++)
                    {
                        Console.WriteLine(String.Format("Starship Name: {0}", starshipResponse.results[i].starshipName));
                        Console.WriteLine();
                        Console.WriteLine(String.Format("JSON Info on Starship: {0}", json.results[i]));
                        Console.WriteLine();
                        Console.WriteLine();
                    }

                    Console.WriteLine();


                    //Outupt entire Json Response
                    Console.WriteLine(json);

                }
            }
            //Handle Errors
            catch(WebException e)
            {
                WebResponse errorResponse = e.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText to a specific file if need be
                }
                throw;
            }
        }
    }
}
